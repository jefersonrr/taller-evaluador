/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import java.util.Iterator;
import ufps.util.colecciones_seed.*;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "<->";
        }
        return msg;
    }

    public String getPrefijo() {

        return null;
    }

    public String getPosfijo() {

       
        Pila<String> p = new Pila();
       
        String msg = "";
        String ultimo = "";

        for (String a : this.expresiones) {
            switch (a) {

                case "(": {
                   
                        p.apilar(a);
                        ultimo = a;
                        
                    
                    break;
                }
            
        
                case "/": {
                    if (sePuedeAgregar(a, ultimo)) {
                         
                        p.apilar(a);
                        ultimo = a;
                        

                    } else {
                        msg += desapilarPila(p);
                        p.apilar(a);
                        ultimo = a;
                    }
                    break;
                }

                case "*": {
                    if (sePuedeAgregar(a, ultimo)) {
                         p.apilar(a);
                        ultimo = a;
                        
                    } else {
                        msg += desapilarPila(p);
                        p.apilar(a);
                        ultimo = a;
                        }
                    break;
                 }
                
                case "+": {
                    if (sePuedeAgregar(a, ultimo)) {

                         p.apilar(a);
                        ultimo = a;
                       
                        

                    } else {
                        msg += desapilarPila(p);
                        p.apilar(a);
                        ultimo = a;
                       
                    }
                    
                    break;
                }

                case "-": {
                    if (!sePuedeAgregar(a, ultimo)) {

                       msg += desapilarPila(p);
                        p.apilar(a);
                        ultimo = a;

                    } else {
                        p.apilar(a);
                        ultimo = a;
                    }
                    break;
                }
                case ")": {
                   msg += desapilarPila(p);
                   
                    ultimo = p.desapilar();
                    p.apilar(ultimo);

                    break;
                }
                default: {
                    msg += a;
                   
                    break;
                    
                }
               
            }
            

        }
        msg += desapilarPila(p);
        return msg;
    }
    
    private String obtenerUltimo(Pila p){
        String ultimo="";
        ultimo = p.desapilar();
    return ultimo;
    
    }

    public boolean sePuedeAgregar(String a, String ultimo) {

        if (a == "/" && (ultimo == "-" || ultimo == "+" || ultimo == ""|| ultimo =="(")) {

            return true;
        }
        if (a == "+" && (ultimo == "" || ultimo=="(") ) {

            return true;
        }
        if (a == "-" &&(ultimo == "" || ultimo=="(") ) {

            return true;
        }

        if (a == "*" && (ultimo == "-" || ultimo == "+" || ultimo == "" || ultimo=="(")) {
            return true;
        }

      

        return false;
    }

    public String desapilarPila(Pila p) {
        String msg = "";
        String d ="";
        boolean cerroCorchete = false;

        while (!p.esVacia() && cerroCorchete==false) {

            d=p.desapilar();
            
            
            if(d=="("){
               
            cerroCorchete=true;
          }else{
            
            msg+= d;}
            System.out.println("Boolean = " + cerroCorchete );
        }
        
      
        return msg;
    }

    public float getEvaluarPosfijo() {
        return 0.0f;
    }

}
